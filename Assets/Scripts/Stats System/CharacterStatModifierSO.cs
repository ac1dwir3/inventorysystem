using UnityEngine;

namespace Ac1dwir3
{
    public abstract class CharacterStatModifierSO : ScriptableObject
    {
        public abstract void AffectCharacter(GameObject character, float value);
    }
}