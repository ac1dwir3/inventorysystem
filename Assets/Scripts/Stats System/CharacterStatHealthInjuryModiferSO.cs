using UnityEngine;

namespace Ac1dwir3
{
    [CreateAssetMenu(fileName = "Health Injury Modifier", menuName = "Stats System/Character Stats/Injury Modifier")]
    public class CharacterStatHealthInjuryModifierSO : CharacterStatModifierSO
    {
        public override void AffectCharacter(GameObject character, float value)
        {
            Debug.Log("Injury heal time decreased by: " + value + "%");
            /*
            if(character.TryGetComponent<Health>(out Health healthSystem))
            {
                healthSystem.IncreaseInjuryHealProcess(Mathf.RoundToInt(value));
            }
        */}
    }
}
