using UnityEngine;

namespace Ac1dwir3
{
    [CreateAssetMenu(fileName = "Health Modifier", menuName = "Stats System/Character Stats/Health Modifier")]
    public class CharacterStatHealthModifierSO : CharacterStatModifierSO
    {
        public override void AffectCharacter(GameObject character, float value)
        {
            Debug.Log("Healed Character for: " + value + " hp");
            /*
            if(character.TryGetComponent<Health>(out Health healthSystem))
            {
                healthSystem.RestoreHealth(Mathf.RoundToInt(value));
            }
        */}
    }
}