using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using UnityEngine;

namespace Ac1dwir3.Inventory.Data
{
    public abstract class ItemSO : ScriptableObject
    {
        public int ID => GetInstanceID();

        [field: SerializeField]
        public bool IsStackable { get; private set; } = false;

        [field: SerializeField]
        public int MaxStackSize { get; private set; } = 1;

        [field: SerializeField]
        public List<ItemParameter> DefaultParametersList { get; private set; }

        [field: Header("UI")]
        [field: SerializeField]
        public string Name { get; private set; }

        [field: SerializeField]
        [field: TextArea]
        public string Description { get; private set; }

        [field: SerializeField]
        public Sprite ItemImage { get; private set; }

        [field: Header("World")]
        [field: SerializeField]
        public GameObject ItemPrefab { get; private set; }
    }

    [Serializable]
    public struct ItemParameter : IEquatable<ItemParameter>, INullable
    {
        public ItemParameterSO parameterData;
        public float value;

        public ItemParameter(ItemParameterSO parameterData, float value)
        {
            this.parameterData = parameterData;
            this.value = value;
        }

        public bool IsNull => parameterData == null;

        public bool Equals(ItemParameter other)
        {
            return this.parameterData == other.parameterData;
        }
    }
}