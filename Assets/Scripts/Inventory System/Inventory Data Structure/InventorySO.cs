using System;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3.Inventory.Data
{
    [CreateAssetMenu(fileName = "New Inventory", menuName = "Inventory System/Inventory")]
    public class InventorySO : ScriptableObject
    {
        [SerializeField]
        public List<InventoryItem> inventoryItems;

        public event Action<Dictionary<int, InventoryItem>> OnInventoryChanged;

        public void Initialize()
        {
            inventoryItems = new List<InventoryItem>();
            RemoveEmptyItems();
        }

        public void RemoveEmptyItems()
        {
            for (int i = inventoryItems.Count - 1; i >= 0; i--)
            {
                if (inventoryItems[i].IsEmpty)
                {
                    inventoryItems.RemoveAt(i);
                }
            }
        }

        public void AddItem(ItemSO item, int quantity, List<ItemParameter> itemState)
        {
            if (item.IsStackable == false)
            {
                while (quantity > 0)
                {
                    quantity -= AddAsNewItemStack(item, 1, itemState);
                }
                InformAboutChange();
            }
            else
            {
                AddStackableItems(item, quantity, itemState);
                InformAboutChange();
            }
        }

        public void AddItem(InventoryItem item)
        {
            AddItem(item.itemData, item.quantity, item.itemState);
        }

        /// <returns>The remaining number of items that were unable to be added to the stack of existing items</returns>
        private int AddStackableItems(ItemSO item, int quantity, List<ItemParameter> itemState)
        {
            for (int i = 0; i < inventoryItems.Count; i++)
            {
                if (inventoryItems[i].itemData.ID != item.ID)
                    continue;

                if (!MatchingItemStates(inventoryItems[i].itemState, itemState))
                    continue;

                int amountPossibleToAddToStack = inventoryItems[i].itemData.MaxStackSize - inventoryItems[i].quantity;

                if (quantity > amountPossibleToAddToStack)
                {
                    inventoryItems[i] = inventoryItems[i].ChangeQuantity(inventoryItems[i].itemData.MaxStackSize);
                    quantity -= amountPossibleToAddToStack;
                }
                else
                {
                    inventoryItems[i] = inventoryItems[i].ChangeQuantity(inventoryItems[i].quantity + quantity);
                    InformAboutChange();
                    return 0;
                }
            }
            while (quantity > 0)
            {
                int newQuantity = Mathf.Clamp(quantity, 0, item.MaxStackSize);
                quantity -= newQuantity;
                AddAsNewItemStack(item, newQuantity, itemState);
            }
            return quantity;
        }

        private bool MatchingItemStates(List<ItemParameter> itemState1, List<ItemParameter> itemState2)
        {
            if (itemState1.Count != itemState2.Count)
                return false;

            for (int i = 0; i < itemState1.Count; i++)
            {
                bool hasParameter = false;
                for (int j = 0; j < itemState2.Count; j++)
                {
                    if (itemState1[i].Equals(itemState2[j]))
                    {
                        hasParameter = true;
                        if (itemState1[i].value != itemState2[j].value)
                        {
                            return false;
                        }
                    }
                }
                if (hasParameter == false)
                    return false;
            }

            return true;
        }

        /// <returns>The remaining number of items that were unable to be added to the stack of new items</returns>
        private int AddAsNewItemStack(ItemSO item, int quantity, List<ItemParameter> itemState = null)
        {
            InventoryItem newItem = new InventoryItem()
            {
                itemData = item,
                quantity = quantity,
                itemState =
                new List<ItemParameter>(itemState ?? item.DefaultParametersList),
            };

            inventoryItems.Add(newItem);
            return quantity;
        }

        public void RemoveItem(int itemIndex, int quantity)
        {
            if (inventoryItems.Count > itemIndex)
            {
                int newQuantity = inventoryItems[itemIndex].quantity - quantity;
                inventoryItems[itemIndex] = inventoryItems[itemIndex].ChangeQuantity(newQuantity);

                if (inventoryItems[itemIndex].IsEmpty)
                    inventoryItems.RemoveAt(itemIndex);

                InformAboutChange();
            }
        }

        public void RemoveItem(ItemSO item, int quantity)
        {
            for (int i = inventoryItems.Count - 1; i >= 0; i--)
            {
                if (inventoryItems[i].itemData.ID == item.ID)
                {
                    RemoveItem(i, quantity);
                    return;
                }
            }
        }

        public void InformAboutChange()
        {
            OnInventoryChanged?.Invoke(GetCurrentInventoryState());
        }

        public InventoryItem GetItemAt(int index)
        {
            return inventoryItems[index];
        }

        public Dictionary<int, InventoryItem> GetCurrentInventoryState()
        {
            Dictionary<int, InventoryItem> returnValue = new Dictionary<int, InventoryItem>();
            for (int i = 0; i < inventoryItems.Count; i++)
            {
                returnValue[i] = inventoryItems[i];
            }
            return returnValue;
        }

    }

    [Serializable]
    public struct InventoryItem
    {
        public int quantity;
        public ItemSO itemData;
        public List<ItemParameter> itemState;

        public InventoryItem(ItemSO _itemData, int _quantity, List<ItemParameter> _itemState)
        {
            itemData = _itemData;
            quantity = _quantity == 0 ? 1 : _quantity;
            itemState = _itemState ?? _itemData.DefaultParametersList;
        }

        public bool IsEmpty => itemData == null || quantity <= 0 || itemState == null;

        public InventoryItem ChangeQuantity(int newQuantity)
        {
            return new InventoryItem
            {
                itemData = this.itemData,
                quantity = newQuantity,
                itemState = new List<ItemParameter>(this.itemState),
            };
        }
    }

}