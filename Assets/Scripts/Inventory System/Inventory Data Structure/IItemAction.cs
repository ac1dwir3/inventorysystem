using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3.Inventory.Data
{
    public interface IItemAction
    {
        public string ActionName { get; } //Displays action name in inventory UI
        public AudioClip ActionSFX { get; } //(OPTIONAL): Sound effects that play when performing action
        public AnimationClip ActionAnimation { get; } //(Optional): Animation that will play when performing action
        bool PerformAction(GameObject character, List<ItemParameter> itemState = null);
    }
}
