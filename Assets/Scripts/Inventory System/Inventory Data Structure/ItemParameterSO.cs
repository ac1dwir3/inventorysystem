using UnityEngine;

namespace Ac1dwir3.Inventory.Data
{
    [CreateAssetMenu(fileName = "New Item Stat", menuName = "Inventory System/Item Stat")]
    public class ItemParameterSO : ScriptableObject
    {
        [field: SerializeField]
        public string ParameterName {  get; private set; }

        [field: SerializeField]
        public float MinValue {  get; private set; }
        [field: SerializeField]
        public float MaxValue {  get; private set; }
    }
}