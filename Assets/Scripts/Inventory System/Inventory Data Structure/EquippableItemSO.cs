using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3.Inventory.Data
{
    [CreateAssetMenu(fileName = "New Equippable Item", menuName = "Inventory System/Item/Equippable Item")]
    public class EquippableItemSO : ItemSO, IDestroyableItem, IItemAction
    {
        public string ActionName => "Equip";

        public AudioClip ActionSFX { get; private set; }

        public AnimationClip ActionAnimation { get; private set; }

        public bool PerformAction(GameObject character, List<ItemParameter> itemState = null)
        {
            Debug.Log("Equipped " + this.Name);
            return true;
            /*if (character.TryGetComponent<EquipmentSystem>(out EquipmentSystem equipmentSystem))
            {
                equipmentSystem.EquipItem(this, itemState ?? DefaultParametersList);
                return true;
            }
            return false;
        */}
    }
}