using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3.Inventory.Data
{
    [CreateAssetMenu(fileName = "New Consumable Item", menuName = "Inventory System/Item/Consumable Item")]
    public class ConsumableItemSO : ItemSO, IDestroyableItem, IItemAction
    {
        [Header("Action")]
        public List<ModifierData> statModifiers = new List<ModifierData>();

        public string ActionName => "Use";

        [field: SerializeField]
        [Tooltip ("OPTOINAL")]
        public AudioClip ActionSFX { get; private set; }

        [field: SerializeField]
        [Tooltip ("OPTOINAL")]
        public AnimationClip ActionAnimation { get; private set; }

        public bool PerformAction(GameObject character, List<ItemParameter> itemState = null)
        {
            Debug.Log($"Used 1 {this.Name}");
            foreach (ModifierData modifierData in statModifiers)
            {
                modifierData.statModifier.AffectCharacter(character, modifierData.value);
            }
            return true;
        }
    }
}