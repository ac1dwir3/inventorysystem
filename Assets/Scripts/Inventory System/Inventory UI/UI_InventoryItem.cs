using Ac1dwir3.Inventory.Data;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Ac1dwir3.Inventory.UI
{
    public class UI_InventoryItem : MonoBehaviour, IPointerClickHandler/*, IPointerEnterHandler, IPointerExitHandler*/
    {
        [SerializeField]
        private Image itemImage;
        [SerializeField]
        private TMP_Text itemNameText;
        [SerializeField]
        private TMP_Text itemTypeText;
        [SerializeField]
        private GameObject itemQuantityObject;
        [SerializeField]
        private TMP_Text quantityText;

        [SerializeField]
        private Image selectionBorderImage;

        public event Action<UI_InventoryItem> OnItemClicked, OnItemRightClicked/*, OnItemEnterHover, OnItemExitHover*/;

        public void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Right)
            {
                OnItemRightClicked?.Invoke(this);
            }
            else if (eventData.button == PointerEventData.InputButton.Left)
            {
                OnItemClicked?.Invoke(this);
            }
        }
        //public void OnPointerEnter(PointerEventData eventData)
        //{
        //    OnItemEnterHover.Invoke(this);
        //}

        //public void OnPointerExit(PointerEventData eventData)
        //{
        //    OnItemExitHover.Invoke(this);
        //}

        public void Select()
        {
            selectionBorderImage.gameObject.SetActive(true);
        }

        public void Deselect()
        {
            selectionBorderImage.gameObject.SetActive(false);
        }

        public void SetItemUIData(ItemSO itemData, int quantity)
        {
            itemImage.sprite = itemData.ItemImage;
            itemNameText.text = itemData.Name;
            quantityText.text = quantity.ToString();
            ShowOrHideQuantity(quantity);
        }
        public void ResetData()
        {
            itemImage.sprite = null;
            itemNameText.text = "";
            quantityText.text = "";
            ShowOrHideQuantity(0);
        }

        private void ShowOrHideQuantity(int quantity)
        {
            if (quantity <= 1)
            {
                itemQuantityObject.SetActive(false);
            }
            else
            {
                itemQuantityObject.SetActive(true);
            }
        }

    }
}