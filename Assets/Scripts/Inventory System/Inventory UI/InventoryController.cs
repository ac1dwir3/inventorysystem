using Ac1dwir3.Inventory.Data;
using Ac1dwir3.Inventory.UI;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3.Inventory
{
    public class InventoryController : MonoBehaviour
    {
        [SerializeField]
        private UI_InventoryMenu inventoryUI;

        [SerializeField]
        private InventorySO inventoryData;
        public InventorySO InventoryData => inventoryData;

        [Header("Testing")]
        [SerializeField]
        private List<InventoryItem> initialInventoryItems;
        [SerializeField]
        private ItemSO testItem;
        [SerializeField]
        private int amountToAdd;
        [SerializeField]
        private int amountToRemove;

        private void Start()
        {
            inventoryData = Instantiate(inventoryData);
            PrepareInventoryData();
            PrepareUI();
        }

        private void PrepareInventoryData()
        {
            inventoryData.Initialize();
            inventoryData.OnInventoryChanged += UpdateInventoryUI;
            ValidateItemData();

            foreach (InventoryItem item in initialInventoryItems)
            {
                inventoryData.AddItem(item);
            }
        }

        [ContextMenu("Validate Inventory List")]
        private void ValidateItemData()
        {
            CondenseInventory();

            for (int index = 0; index < initialInventoryItems.Count; index++)
            {
                ValidateItemQuantity(index);
                ValidateItemParameterValues(index);
            }
        }
        private void ValidateItemQuantity(int index)
        {
            if (initialInventoryItems[index].itemData.IsStackable)
            {
                if (initialInventoryItems[index].quantity > initialInventoryItems[index].itemData.MaxStackSize)
                {
                    Debug.Log($"{initialInventoryItems[index].itemData.Name} at index {index} is initialized at a stack size larger than its maximum ({initialInventoryItems[index].quantity}/{initialInventoryItems[index].itemData.MaxStackSize}). Reducing stack to maximum size.");
                    initialInventoryItems[index] = new InventoryItem(initialInventoryItems[index].itemData, initialInventoryItems[index].itemData.MaxStackSize, initialInventoryItems[index].itemState);
                }
            }
            else
            {
                if (initialInventoryItems[index].quantity != 1)
                {
                    Debug.Log($"{initialInventoryItems[index].itemData.Name} is not stackable yet is initialized at a stack size larger than 1 ({initialInventoryItems[index].quantity}/1). Reducing stack to 1.");
                    initialInventoryItems[index] = new InventoryItem(initialInventoryItems[index].itemData, 1, initialInventoryItems[index].itemState);
                }
            }
        }
        private void ValidateItemParameterValues(int index)
        {
            for (int j = 0; j < initialInventoryItems[index].itemState.Count; j++)
            {
                if (initialInventoryItems[index].itemState[j].value > initialInventoryItems[index].itemState[j].parameterData.MaxValue)
                {
                    Debug.Log($"{initialInventoryItems[index].itemState[j].parameterData.ParameterName} of item \"{initialInventoryItems[index].itemData.Name}\" at index {index} is initialized at a value larger than its maximum ({initialInventoryItems[index].itemState[j].value}/{initialInventoryItems[index].itemState[j].parameterData.MaxValue}). Reducing value to maximum value.");
                    initialInventoryItems[index].itemState[j] = new ItemParameter(initialInventoryItems[index].itemState[j].parameterData, initialInventoryItems[index].itemState[j].parameterData.MaxValue);
                }
                else if (initialInventoryItems[index].itemState[j].value < initialInventoryItems[index].itemState[j].parameterData.MinValue)
                {
                    Debug.Log($"{initialInventoryItems[index].itemState[j].parameterData.ParameterName} of item \"{initialInventoryItems[index].itemData.Name}\" at index {index} is initialized at a value smaller than its minimum ({initialInventoryItems[index].itemState[j].value}/{initialInventoryItems[index].itemState[j].parameterData.MinValue}). Increasing value to minimum value.");
                    initialInventoryItems[index].itemState[j] = new ItemParameter(initialInventoryItems[index].itemState[j].parameterData, initialInventoryItems[index].itemState[j].parameterData.MinValue);
                }
            }
        }
        public void CondenseInventory()
        {
            MergeRedundantStacks();
            RemoveEmptyItems();
        }
        public void RemoveEmptyItems()
        {
            for (int i = initialInventoryItems.Count - 1; i >= 0; i--)
            {
                if (initialInventoryItems[i].IsEmpty)
                {
                    initialInventoryItems.RemoveAt(i);
                }
            }
        }
        public void MergeRedundantStacks()
        {
            for (int i = 0; i < initialInventoryItems.Count - 1; i++)
            {
                if (!initialInventoryItems[i].itemData.IsStackable || initialInventoryItems[i].quantity == initialInventoryItems[i].itemData.MaxStackSize)
                    continue;

                for (int j = i + 1; j < initialInventoryItems.Count; j++)
                {
                    if (initialInventoryItems[j].itemData.ID != initialInventoryItems[i].itemData.ID)
                        continue;

                    int amountPossibleToAddToStack = initialInventoryItems[i].itemData.MaxStackSize - initialInventoryItems[i].quantity;

                    if (initialInventoryItems[j].quantity > amountPossibleToAddToStack)
                    {
                        initialInventoryItems[i] = initialInventoryItems[i].ChangeQuantity(initialInventoryItems[i].itemData.MaxStackSize);
                        initialInventoryItems[j] = initialInventoryItems[j].ChangeQuantity(initialInventoryItems[j].quantity - amountPossibleToAddToStack);
                    }
                    else
                    {
                        initialInventoryItems[i] = initialInventoryItems[i].ChangeQuantity(initialInventoryItems[i].quantity + initialInventoryItems[j].quantity);
                        initialInventoryItems[j] = initialInventoryItems[j].ChangeQuantity(0);
                    }
                }
            }
        }

        private void PrepareUI()
        {
            inventoryData.OnInventoryChanged += inventoryUI.RefreshInventoryUI;
            inventoryUI.RefreshInventoryUI(inventoryData.GetCurrentInventoryState());
            inventoryUI.OnItemInformationRequested += HandleInformationRequest;
            inventoryUI.OnItemActionRequested += HandleItemActionRequest;
        }

        private void UpdateInventoryUI(Dictionary<int, InventoryItem> inventoryState)
        {
            inventoryUI.ResetAllItems();
            UpdateInventoryUIData(inventoryState);
        }

        private void UpdateInventoryUIData(Dictionary<int, InventoryItem> inventoryState)
        {
            foreach (var item in inventoryState)
            {
                inventoryUI.UpdateData(item.Key, item.Value.itemData, item.Value.quantity);
            }
        }
        private void UpdateInventoryUIData()
        {
            UpdateInventoryUIData(inventoryData.GetCurrentInventoryState());
        }

        private void HandleInformationRequest(int itemIndex)
        {
            InventoryItem item = inventoryData.GetItemAt(itemIndex);
            inventoryUI.UpdateInfoView(itemIndex, item.itemData, item.itemState);
        }

        private void HandleItemActionRequest(int itemIndex)
        {
            InventoryItem item = inventoryData.GetItemAt(itemIndex);
            inventoryUI.UpdateInfoView(itemIndex, item.itemData, item.itemState);

            IItemAction itemAction = item.itemData as IItemAction;
            if (itemAction != null)
            {
                inventoryUI.ShowItemActions(itemIndex);
                inventoryUI.AddAction(itemAction.ActionName, () => PerformAction(itemIndex));
            }

            IDestroyableItem destroyableItem = item.itemData as IDestroyableItem;
            if (destroyableItem != null)
            {
                inventoryUI.AddAction("Drop", () => DropItem(itemIndex, item.quantity));
            }
        }
        public void DropItem(int index, int quantity)
        {
            Debug.Log($"Dropped stack of {quantity} {inventoryData.GetItemAt(index).itemData.Name}s");
            inventoryData.RemoveItem(index, quantity);
            inventoryUI.ResetSelection();
            //Spawn item's world prefab on the ground
        }

        public void PerformAction(int itemIndex)
        {
            InventoryItem item = inventoryData.GetItemAt(itemIndex);
            if (item.IsEmpty)
                return;

            IDestroyableItem destroyableItem = item.itemData as IDestroyableItem;
            if (destroyableItem != null)
            {
                inventoryData.RemoveItem(itemIndex, 1);
            }

            IItemAction itemAction = item.itemData as IItemAction;
            if (itemAction != null)
            {
                itemAction.PerformAction(gameObject);
                inventoryUI.ResetSelection();
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.I))
            {
                if (!inventoryUI.isActiveAndEnabled)
                {
                    inventoryUI.Show();
                    UpdateInventoryUIData();
                }
                else
                {
                    inventoryUI.Hide();
                }
            }

            if (Input.GetKeyDown(KeyCode.KeypadPlus))
            {
                inventoryData.AddItem(testItem, amountToAdd, null);
            }

            if (Input.GetKeyDown(KeyCode.KeypadMinus))
            {
                inventoryData.RemoveItem(testItem, amountToRemove);
            }
        }
    }
}
