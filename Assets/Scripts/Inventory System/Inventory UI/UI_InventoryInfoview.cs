using Ac1dwir3.Inventory.Data;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ac1dwir3.Inventory.UI
{
    public class UI_InventoryInfoview : MonoBehaviour
    {
        [SerializeField]
        private Image itemView;
        [SerializeField]
        private TMP_Text itemName;
        [SerializeField]
        private TMP_Text itemDescription;

        [Header("Stats View")]
        [SerializeField]
        private UI_ItemStatBar uiItemStatPrefab;
        [SerializeField]
        private RectTransform itemStatsPanel;

        List<UI_ItemStatBar> listOfUIStats = new List<UI_ItemStatBar>();

        private void Awake()
        {
            ResetInfoview();
        }

        public void ResetInfoview()
        {
            itemView.gameObject.SetActive(false);
            itemView.sprite = null;
            itemName.text = "";
            itemDescription.text = "";
            itemStatsPanel.gameObject.SetActive(false);
            DeleteAllUIStats();
        }

        private void RefreshStatView(List<ItemParameter> itemState)
        {
            DeleteAllUIStats();
            InstantiateNewUIStats(itemState);
        }

        private void DeleteAllUIStats()
        {
            for (int i = 0; i < listOfUIStats.Count; i++)
            {
                Destroy(itemStatsPanel.GetChild(i).gameObject);
            }
            listOfUIStats.Clear();
        }

        private void InstantiateNewUIStats(List<ItemParameter> itemState)
        {
            for (int i = 0; i < itemState.Count; i++)
            {
                UI_ItemStatBar UIStat = Instantiate(uiItemStatPrefab, itemStatsPanel, false);
                UIStat.SetStatBarUIData(itemState[i]);
                listOfUIStats.Add(UIStat);
            }
        }

        public void SetItemInfo(Sprite sprite, string name, string description, List<ItemParameter> itemState = null)
        {
            itemView.gameObject.SetActive(true);
            itemView.sprite = sprite;
            itemName.text = name;
            itemDescription.text = description;
            ShowOrHideStatView(itemState);
        }

        private void ShowOrHideStatView(List<ItemParameter> itemState)
        {
            if (itemState.Count <= 0)
                itemStatsPanel.gameObject.SetActive(false);
            else
            {
                itemStatsPanel.gameObject.SetActive(true);
                RefreshStatView(itemState);
            }
        }

        public void SetItemInfo(ItemSO itemData, List<ItemParameter> itemState = null)
        {
            SetItemInfo(itemData.ItemImage, itemData.Name, itemData.Description, itemState ?? itemData.DefaultParametersList);
        }
    }
}