using Ac1dwir3.Inventory.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ac1dwir3.Inventory.UI
{
    public class UI_ItemStatBar : MonoBehaviour
    {
        [SerializeField]
        private TMP_Text statNameText;
        [SerializeField]
        private TMP_Text statValueText;
        [SerializeField]
        private Slider statSlider;

        public void SetStatBarUIData(ItemParameter itemParameter)
        {
            SetStatBarUIData(itemParameter.parameterData.ParameterName, itemParameter.value, itemParameter.parameterData.MinValue, itemParameter.parameterData.MaxValue);
        }
        public void SetStatBarUIData(string statName, float currentValue, float minValue, float maxValue)
        {
            statNameText.text = statName;
            statSlider.minValue = minValue;
            statSlider.maxValue = maxValue;
            statSlider.value = currentValue;
            statValueText.text = currentValue.ToString();
        }
    }
}