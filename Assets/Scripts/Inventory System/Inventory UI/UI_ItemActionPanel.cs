using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Ac1dwir3.Inventory.UI
{
    public class UI_ItemActionPanel : MonoBehaviour
    {
        [SerializeField]
        private GameObject itemActionButtonPrefab;

        public void AddButton(string name, Action onClickAction)
        {
            GameObject button = Instantiate(itemActionButtonPrefab, transform, false);
            button.GetComponentInChildren<Button>().onClick.AddListener(() => onClickAction());
            button.GetComponentInChildren<TMP_Text>().text = name;
        }

        public void Toggle(bool toggle)
        {
            if (toggle == true)
                RemoveAllButton();
            gameObject.SetActive(toggle);
        }

        private void RemoveAllButton()
        {
            foreach(Transform childObjects in transform)
            {
                Destroy(childObjects.gameObject);
            }
        }
    }
}