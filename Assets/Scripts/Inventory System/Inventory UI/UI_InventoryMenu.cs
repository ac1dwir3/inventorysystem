using Ac1dwir3.Inventory.Data;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Ac1dwir3.Inventory.UI
{
    public class UI_InventoryMenu : MonoBehaviour
    {
        [SerializeField]
        private UI_InventoryItem uiItemPrefab;

        [SerializeField]
        private RectTransform contentPanel;
        [SerializeField]
        private UI_InventoryInfoview infoview;
        [SerializeField]
        private UI_ItemActionPanel actionPanel;

        List<UI_InventoryItem> listOfUIItems = new List<UI_InventoryItem>();

        public event Action<int> OnItemInformationRequested, OnItemActionRequested;

        private Canvas canvas;

        private void Awake()
        {
            //Hide();
            infoview.ResetInfoview();
            canvas = GetComponentInParent<Canvas>();
        }

        public void RefreshInventoryUI(Dictionary<int, InventoryItem> inventoryState)
        {
            UpdateExistingUIItems(inventoryState);
            InstantiateNewUIItems(inventoryState);
        }

        public void UpdateExistingUIItems(Dictionary<int, InventoryItem> inventoryState)
        {
            for (int i = 0; i < listOfUIItems.Count; i++)
            {
                if (!inventoryState.ContainsKey(i))
                {
                    DeleteUIItem(i);
                    continue;
                }
                UpdateData(i, inventoryState[i].itemData, inventoryState[i].quantity);
            }
        }
        private void DeleteUIItem(int index)
        {
            listOfUIItems.RemoveAt(index);
            Destroy(contentPanel.GetChild(index).gameObject);
        }
        private void InstantiateNewUIItems(Dictionary<int, InventoryItem> inventoryState)
        {
            for (int i = listOfUIItems.Count; i < inventoryState.Count; i++)
            {
                UI_InventoryItem UIItem = Instantiate(uiItemPrefab, contentPanel, false);
                UIItem.SetItemUIData(inventoryState[i].itemData, inventoryState[i].quantity);
                UIItem.OnItemClicked += HandleItemSelection;
                UIItem.OnItemRightClicked += HandleShowItemActions;
                listOfUIItems.Add(UIItem);
            }
        }

        public void UpdateInfoView(int itemIndex, ItemSO itemData, List<ItemParameter> itemState = null)
        {
            infoview.SetItemInfo(itemData, itemState);
            DeselectAllItems();
            listOfUIItems[itemIndex].Select();
        }

        public void UpdateData(int itemIndex, ItemSO itemData, int itemQuantity)
        {
            if (listOfUIItems.Count > itemIndex)
            {
                listOfUIItems[itemIndex].SetItemUIData(itemData, itemQuantity);
            }
        }

        public void ResetAllItems()
        {
            foreach (UI_InventoryItem UIItem in listOfUIItems)
            {
                UIItem.ResetData();
                UIItem.Deselect();
            }
        }

        private void HandleItemSelection(UI_InventoryItem UIItem)
        {
            int index = listOfUIItems.IndexOf(UIItem);
            OnItemInformationRequested?.Invoke(index);
        }

        private void DeselectAllItems()
        {
            for (int i = 0; i < listOfUIItems.Count; i++)
            {
                listOfUIItems[i].Deselect();
            }
            actionPanel.Toggle(false);
        }

        public void ResetSelection()
        {
            infoview.ResetInfoview();
            DeselectAllItems();
        }

        public void ShowItemActions(int itemIndex)
        {
            actionPanel.Toggle(true);
            RectTransformUtility.ScreenPointToLocalPointInRectangle((RectTransform)canvas.transform, Input.mousePosition, canvas.worldCamera, out Vector2 mousePosition);
            actionPanel.transform.position = canvas.transform.TransformPoint(mousePosition);
        }

        public void AddAction(string actionName, Action performAction)
        {
            actionPanel.AddButton(actionName, performAction);
        }

        private void HandleShowItemActions(UI_InventoryItem UIItem)
        {
            GetIndexOfItem(UIItem, out int index);
            OnItemActionRequested?.Invoke(index);
        }

        private int GetIndexOfItem(UI_InventoryItem UIItem, out int index)
        {
            index = listOfUIItems.IndexOf(UIItem);
            return index;
        }

        public void Show()
        {
            gameObject.SetActive(true);
            infoview.ResetInfoview();
        }

        public void Hide()
        {
            actionPanel.Toggle(false);
            gameObject.SetActive(false);
        }
    }
}