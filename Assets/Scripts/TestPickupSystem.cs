using Ac1dwir3.Inventory;
using Ac1dwir3.Inventory.Data;
using UnityEngine;

namespace Ac1dwir3
{
    public class TestPickupSystem : MonoBehaviour
    {
        [SerializeField]
        private InventoryController InventoryController;

        [Header("Testing")]
        [SerializeField]
        private ItemSO testItem;
        [SerializeField]
        private int testQuantity;

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                InventoryController.InventoryData.AddItem(testItem, testQuantity, null);
            }
        }
    }
}